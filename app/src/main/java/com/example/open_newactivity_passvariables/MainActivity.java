package com.example.open_newactivity_passvariables;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText text1, text2;
    public static final String Extra_Text = "com.example.open_newactivity_passvariables.Extra_Text";
    public static final String Extra_Number = "com.example.open_newactivity_passvariables.Extra_Number";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.button);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = text1.getText().toString();
                int number = Integer.parseInt((text2.getText().toString()));
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);

                intent.putExtra(Extra_Text, text);
                intent.putExtra(Extra_Number, number);
                startActivity(intent);
            }
        });

    }
}
