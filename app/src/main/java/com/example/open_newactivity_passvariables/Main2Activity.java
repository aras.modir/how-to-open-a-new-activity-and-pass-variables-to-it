package com.example.open_newactivity_passvariables;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
    TextView textView1, textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        textView1 = findViewById(R.id.textview1);
        textView2 = findViewById(R.id.textview2);

        Intent intent = getIntent();
        String text = intent.getStringExtra(MainActivity.Extra_Text);
        int number = intent.getIntExtra(MainActivity.Extra_Number, 0);

        textView1.setText(text);
        textView2.setText("" + number);

    }
}
